from django.contrib import admin

from .models import Category


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ["name", "parent", "created_time"]
    list_filter = ['created_time']
    search_fields = ["name"]
    raw_id_fields = ["parent"]
    list_select_related = ['parent']
