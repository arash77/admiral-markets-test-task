from django.db import models
from django.utils.translation import ugettext_lazy as _


class Category(models.Model):
    """
    Category Model.
    """
    created_time = models.DateTimeField(_('created time'), auto_now_add=True)
    updated_time = models.DateTimeField(_('updated time'), auto_now=True)
    name = models.CharField(_('name'), max_length=50, unique=True)
    parent = models.ForeignKey('self', on_delete=models.PROTECT, related_name='children', null=True, blank=True)

    class Meta:
        db_table = 'categories'
        verbose_name = _('category')
        verbose_name_plural = _('categories')
        ordering = ['name']

    def __str__(self):
        return self.name

    def siblings(self):
        """ get siblings of current this category excluding itself """
        return Category.objects.filter(parent=self.parent).exclude(pk=self.pk)

    def parents(self):
        """
        get parents of current category
            basically this has a lot of extra queries but nothing better came to my mind
        """
        instance = self
        parents = []
        while instance.parent is not None:
            parents.append(instance.parent)
            instance = instance.parent
        return parents
