from django.test import TestCase
from ..models import Category


class CategoryTest(TestCase):
    """ Test module for Category model """

    def setUp(self):
        c = Category.objects.create(name='Category 1', parent=None)
        Category.objects.create(name='Category 1.1', parent=c)

    def test_category_hierarchy(self):
        parent = Category.objects.get(name='Category 1')
        child = Category.objects.get(name='Category 1.1')
        self.assertEqual(child.parent, parent)
