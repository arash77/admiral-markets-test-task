from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase

from ..models import Category


class CategoryTests(APITestCase):
    sample_data = {
        "name": "Category 1",
        "children": [
            {
                "name": "Category 1.1",
                "children": [
                    {
                        "name": "Category 1.1.1",
                        "children": [
                            {
                                "name": "Category 1.1.1.1"
                            },
                            {
                                "name": "Category 1.1.1.2"
                            },
                            {
                                "name": "Category 1.1.1.3"
                            }
                        ]
                    },
                    {
                        "name": "Category 1.1.2",
                        "children": [
                            {
                                "name": "Category 1.1.2.1"
                            },
                            {
                                "name": "Category 1.1.2.2"
                            },
                            {
                                "name": "Category 1.1.2.3"
                            }
                        ]
                    }
                ]
            },
            {
                "name": "Category 1.2",
                "children": [
                    {
                        "name": "Category 1.2.1"
                    },
                    {
                        "name": "Category 1.2.2",
                        "children": [
                            {
                                "name": "Category 1.2.2.1"
                            },
                            {
                                "name": "Category 1.2.2.2"
                            }
                        ]
                    }
                ]
            }
        ]
    }

    def setUp(self):
        self.create_response = self.client.post(reverse('category-list'), self.sample_data, format='json')

    def test_create_category_hierarchy(self):
        """
        Ensure we can create category hierarchy.
        """
        # response = self.client.post(reverse('category-list'), self.sample_data, format='json')
        self.assertEqual(self.create_response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Category.objects.count(), 15)
        self.assertEqual(Category.objects.get(name='Category 1.1.1').children.count(), 3)
        self.assertEqual(Category.objects.get(name='Category 1.1.1').parent.name, 'Category 1.1')

    def test_get_valid_category(self):
        """
        Ensure retrieve API works.
        """
        c = Category.objects.get(name='Category 1.1')
        response = self.client.get(reverse('category-detail', kwargs={'pk': c.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['parents']), 1)
        self.assertEqual(len(response.data['children']), 2)
        self.assertEqual(len(response.data['siblings']), 1)
