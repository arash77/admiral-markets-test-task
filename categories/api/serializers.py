from rest_framework import serializers

from ..models import Category


class SimpleCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ['id', 'name']


class CategorySerializer(serializers.ModelSerializer):
    parents = SimpleCategorySerializer(read_only=True, many=True)
    children = SimpleCategorySerializer(read_only=True, many=True)
    siblings = SimpleCategorySerializer(read_only=True, many=True)

    class Meta:
        model = Category
        fields = ['id', 'name', 'parents', 'children', 'siblings',]


class CategoryHierarchyCreateSerializer(serializers.ModelSerializer):
    """
    This serializer get category tree as an input
    """
    class Meta:
        model = Category
        fields = ['id', 'name', 'children']
        read_only_fields = ['id']
        extra_kwargs = {
            'name': {'allow_blank': False},
        }

    def get_fields(self):
        fields = super().get_fields()
        fields['children'] = CategoryHierarchyCreateSerializer(many=True, required=False)
        return fields

    def create(self, validated_data):
        """ Recursively create categories """
        children = validated_data.pop('children', [])
        instance = super().create(validated_data)
        for c in children:
            c['parent_id'] = instance.id
            instance.children.add(self.create(c))
        return instance
