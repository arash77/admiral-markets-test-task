from rest_framework import viewsets, mixins, status
from rest_framework.response import Response

from ..models import Category
from .serializers import CategorySerializer, CategoryHierarchyCreateSerializer


class CategoryViewSet(mixins.CreateModelMixin,
                      mixins.RetrieveModelMixin,
                      viewsets.GenericViewSet):
    """
    ViewSet for creating and retrieving categories.
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

    def create(self, request, *args, **kwargs):
        serializer = CategoryHierarchyCreateSerializer(data=request.data)

        if serializer.is_valid():
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

        err_msg = self.get_error(serializer.errors, serializer.initial_data)
        return Response({'error': err_msg}, status=status.HTTP_400_BAD_REQUEST)

    def get_error(self, error_hierarchy, data):
        """
        Let the client know which object caused the error.
        For unknown cases the representation of error is displayed.
        This method caches the first error in hierarchy tree
        """
        if error_hierarchy.get('name') is not None:
            e = error_hierarchy['name'][0]

            err_msg ={
                # override message `category with this name already exists.`
                'unique': f"category `{data['name']}` already exists!",
                # if some object does not have a name value
                'required': f"object {data}: name is required!",
                # if name is blank
                'blank': f"category name can not be blank!",
            }.get(e.code, f'{data}: {error_hierarchy}')

            return err_msg

        if error_hierarchy.get('children') is not None:
            for i, v in enumerate(error_hierarchy['children']):
                return self.get_error(v, data['children'][i])
