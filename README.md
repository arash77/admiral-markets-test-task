# Admiral Markets Test Task
A Test Task for Admiral Markets written by Arash Fazeli

## Setup
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install requirements
```bash
$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

##### Setup environment file
create `.env` file in the root directory of the project and set the following settings.
```python
# set to False on production
DEBUG = True 
# set only in development mode
DEVEL = True

ALLOWED_HOSTS = '*'

SECRET_KEY = 'your_secret_key'

# Database engine & connection configs 
DB_ENGINE = 'django.db.backends.sqlite3'
DB_NAME = 'db.sqlite3'
DB_USER = ''
DB_PASS = ''
DB_HOST = ''
DB_PORT = ''
```
Save and exit the file
```bash
$ python manage.py check
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py test
$ python manage.py runserver
```

## Usage
Send POST request to `http://localhost:8000/api/v1/categories/`  
Retrieve GET categories via `http://localhost:8000/api/v1/categories/:category_id/`  